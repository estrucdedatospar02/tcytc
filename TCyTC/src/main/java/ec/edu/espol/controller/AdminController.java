/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.util.*;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.*;
import org.apache.log4j.Logger;

/**
 * FXML Controller class
 *
 * @author Andrés Medina Jácome
 */
public class AdminController implements Initializable {

    
    /**
     * Initializes the controller class.
     */
    
    @FXML
    private Button btn1;
    @FXML
    private Button btn2;
    @FXML
    private Button btn3;
    @FXML
    private Button btn4;
    
    private Stage scene;
    private PantallaEsperaController pe; 
    private DoublyCircularLinkedList<String> l;
    private static final String TITULO = "TCyTC - Ventana externa v1.1.2020";
   
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        /*
         * Desde el controlador AdminController se envía los atributos
         * por medio del método pasarPE()
         *
        */
    }    

    private void pasarPE(){
        try {
            FXMLLoader fmxl = Sistema.loadFXML("pantallaEspera");
            Parent form = fmxl.load();
            pe = fmxl.getController();
            Stage st = new Stage();
            Scene sc = new Scene(form);
            st.setScene(sc);
            pe.setAtributtes(l,scene);
            st.initStyle(StageStyle.UNDECORATED);
            st.show();
        } catch (IOException ex) {
            Logger logger = Logger.getLogger(AdminController.class);
            logger.error(ex.getMessage());
        }
    }
    
    @FXML
    private void cerrarPrograma(ActionEvent event) {
        System.exit(0);
    }
    
    public void setAtributtes(Stage scene){
        try {
            this.scene=scene;
            l = Video.cargarVideos();
            pasarPE();
        } catch (IOException ex) {
            Sistema.showMessage(new Alert(Alert.AlertType.WARNING), "ERROR", "Error al cargar archivo", ex.getMessage());
            Logger logger = Logger.getLogger(AdminController.class);
            logger.error(ex.getMessage());
        }
    }

    @FXML
    private void registrarPaciente(MouseEvent event) {
        try {
            FXMLLoader loader = Sistema.loadFXML("registrarPaciente");
            Parent root = loader.load();
            RegistrarPacienteController control = loader.getController();
            control.setPec(pe);
            mostrarPantalla(root);
        } catch (IOException ex) {
            Logger logger = Logger.getLogger(AdminController.class);
            logger.error(ex.getMessage());
        }
    }

    @FXML
    private void registrarMedico(MouseEvent event) {
        try {
            FXMLLoader loader = Sistema.loadFXML("registrarMedico");
            Parent root = loader.load();
            mostrarPantalla(root);
        } catch (IOException ex) {
            Logger logger = Logger.getLogger(AdminController.class);
            logger.error(ex.getMessage());
        }
    }

    @FXML
    private void asignarPuesto(MouseEvent event) {
        try {
            FXMLLoader loader = Sistema.loadFXML("asignarPuesto");
            Parent root = loader.load();
            AsignarPuestoController control = loader.getController();
            control.setPec(pe);
            mostrarPantalla(root);
        } catch (IOException ex) {
            Logger logger = Logger.getLogger(AdminController.class);
            logger.error(ex.getMessage());
        }
    }
    
    @FXML
    private void atenderPaciente(MouseEvent event) {
        try {
            FXMLLoader loader = Sistema.loadFXML("atenderPaciente");
            Parent root = loader.load();
            AtenderPacienteController control = loader.getController();
            control.setPec(pe);
            mostrarPantalla(root);
        } catch (IOException ex) {
            Logger logger = Logger.getLogger(AdminController.class);
            logger.error(ex.getMessage());
        }
    }
    
    private void mostrarPantalla(Parent root) throws IOException{
        Scene sc = new Scene(root);
        Stage stage = new Stage();
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.getIcons().add(new Image("/img/tcytc.png"));
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.setTitle(TITULO);
        stage.setScene(sc);
        stage.showAndWait();
    }
    
    @FXML
    private void presentarP(ActionEvent event) {
        try {
            FXMLLoader loader = Sistema.loadFXML("mostrarPaciente");
            Parent root = loader.load();
            mostrarPantalla(root);
        } catch (IOException ex) {
            Logger logger = Logger.getLogger(AdminController.class);
            logger.error(ex.getMessage());
        }
    }

    @FXML
    private void presentarM(ActionEvent event) {
        try {
            FXMLLoader loader = Sistema.loadFXML("mostrarMedico");
            Parent root = loader.load();
            mostrarPantalla(root);
        } catch (IOException ex) {
            Logger logger = Logger.getLogger(AdminController.class);
            logger.error(ex.getMessage());
        }
    }
}