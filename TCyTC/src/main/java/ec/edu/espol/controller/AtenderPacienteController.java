/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.*;
import ec.edu.espol.util.*;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Andrés Medina Jácome
 */
public class AtenderPacienteController implements Initializable {

    @FXML
    private ComboBox<Turno> puestos;
    @FXML
    private TextArea infoPaciente;

    private PantallaEsperaController pec;
    
    @FXML
    private VBox listReceta;
    @FXML
    private TextArea diagnostico;
    @FXML
    private Button quiR;
    @FXML
    private TextField txR;
    @FXML
    private Button agrR;

    public PantallaEsperaController getPec() {
        return pec;
    }

    public void setPec(PantallaEsperaController pec) {
        this.pec = pec;
        puestos.setItems(FXCollections.observableList(pec.getLturnos()));
    }
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        txR.setStyle("-fx-background-color: #A8FF79; -fx-border-color: #000000; -fx-background-radius: 100; -fx-border-radius: 100");
    }    

    @FXML
    private void seleccion(ActionEvent event) {
        infoPaciente.setText(puestos.getValue().getPaciente().toString());
        agrR.setDisable(false);
        quiR.setDisable(false);
    }

    @FXML
    private void finalizar(MouseEvent event) {
        try {
            if(infoPaciente.getText().equals("")) throw new ExistenceException("No ha seleccionado un puesto");
            else if(diagnostico.getText().equals("")) throw new ExistenceException("Llene el cuadro de diagnóstico");
            LinkedList<String> listaRecetas = new LinkedList<>();
            for(Node t : listReceta.getChildren()){
                TextField tx = (TextField) t;
                if(tx.getText().equals(""))    throw new ExistenceException("Llene el cuadro de la receta");
                listaRecetas.add(tx.getText());
            }finalizarCita(pec,puestos,diagnostico,listaRecetas);
            Stage st = (Stage) quiR.getScene().getWindow();
            st.close();
        } catch (ExistenceException | IOException | ClassNotFoundException ex) {
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Impresion de receta", ex.getMessage());
        }
    }

    private static void finalizarCita(PantallaEsperaController pec, ComboBox<Turno> puestos,TextArea diagnostico,LinkedList<String> listaRecetas) throws ExistenceException, IOException, ClassNotFoundException{
        LinkedList<Paciente> lPaciente = (LinkedList<Paciente>) Lectura.leerPacientes();
        Paciente paciente = lPaciente.get(lPaciente.indexOf(puestos.getValue().getPaciente()));
        Puesto puesto = puestos.getValue().getPuesto();
        Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Impresión Receta", puesto.getMedico().atenderPaciente(paciente, listaRecetas,diagnostico.getText()));
        Proteccion.guardarPaciente(lPaciente);
        pec.pacienteAtendido(puestos.getValue());
    }
    
    
    @FXML
    private void agregarR(MouseEvent event) {
        TextField txt = new TextField();
        txt.setPromptText("Medicamento "+(listReceta.getChildren().size()+1));
        txt.setStyle("-fx-background-color: #E0F6F9; -fx-border-color: #000000; -fx-background-radius: 100; -fx-border-radius: 100");
        listReceta.getChildren().add(txt);
    }

    @FXML
    private void quitarR(MouseEvent event) {
        listReceta.getChildren().remove(listReceta.getChildren().size()-1);
    }
    
}

