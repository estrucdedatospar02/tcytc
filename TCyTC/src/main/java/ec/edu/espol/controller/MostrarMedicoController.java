/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.Medico;
import ec.edu.espol.util.Lectura;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import javafx.collections.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.apache.log4j.Logger;

/**
 * FXML Controller class
 *
 * @author Andrés Medina Jácome
 */
public class MostrarMedicoController implements Initializable {

    @FXML
    private TableView tbPaciente;
    @FXML
    private TableColumn cNombre;
    @FXML
    private TableColumn cApellido;
    @FXML
    private TableColumn cEdad;
    @FXML
    private TableColumn cGenero;
    
    @FXML
    private TableColumn cID;
    @FXML
    private TableColumn cEspecialidad;
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ObservableList<Medico> medicos = FXCollections.observableArrayList();
            LinkedList<Medico> lmedicos = (LinkedList<Medico>) Lectura.leerMedicos();
            medicos.addAll(lmedicos);
            this.cNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
            this.cApellido.setCellValueFactory(new PropertyValueFactory("apellido"));
            this.cEdad.setCellValueFactory(new PropertyValueFactory("edad"));
            this.cGenero.setCellValueFactory(new PropertyValueFactory("genero"));
            this.cEspecialidad.setCellValueFactory(new PropertyValueFactory("especialidad"));
            this.cID.setCellValueFactory(new PropertyValueFactory("id"));
            this.tbPaciente.setItems(medicos);
        } catch (IOException | ClassNotFoundException ex) {
            Logger logger = Logger.getLogger(MostrarMedicoController.class);
            logger.error(ex.getMessage());
        }
    }    
    
}
