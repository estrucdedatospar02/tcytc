/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.*;
import ec.edu.espol.util.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.*;
import javafx.fxml.*;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.*;
import javafx.scene.media.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

/**
 * FXML Controller class
 *
 * @author Andrés Medina Jácome
 */
public class PantallaEsperaController implements Initializable {

    @FXML
    private MediaView mediaV;
    @FXML
    private ImageView ico;
    @FXML
    private Label turnoP;
    @FXML
    private Label turnoS;
    @FXML
    private Label turnoU;
    @FXML
    private Text tiempo;
    @FXML
    private Label puestoP;
    @FXML
    private Label puestoS;
    @FXML
    private Label puestoU;
    @FXML
    private Label notif;
    
    private Stage ac;
    private MediaPlayer md;
    private PriorityQueue<Paciente> pq = new PriorityQueue<>((Paciente p1, Paciente p2) -> (p1.compareTo(p2)));
    private Queue<Puesto> lpuestos;
    private ArrayList<Turno> lturnos;
    private DoublyCircularLinkedList<String> listVid;
    private static final String FONDOA = "-fx-background-color: #00FF44";
    private static final String FONDOB = "-fx-background-color: #00FF91";
    private int cA;
    private int cB;
    private int cC;
    private int cD;
    private int cE;
    private int cF;

    public Queue<Puesto> getLpuestos() {
        return lpuestos;
    }

    public void setLpuestos(Queue<Puesto> lpuestos) {
        this.lpuestos = lpuestos;
    }

    public ArrayList<Turno> getLturnos() {
        return lturnos;
    }

    public void setLturnos(ArrayList<Turno> lturnos) {
        this.lturnos = lturnos;
    }
    
        
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            LinkedList<Paciente> pacientes = (LinkedList<Paciente>) Lectura.leerPacientes();
            if(!pacientes.isEmpty())   pq.addAll(pacientes);
            lpuestos = (LinkedList<Puesto>) Lectura.leerPuestos();
            lturnos = new ArrayList<>();
            actualizarTurnos();
        } catch (IOException | ClassNotFoundException ex) {
            Logger logger = Logger.getLogger(PantallaEsperaController.class);
            logger.error(ex.getMessage());
        }
    }    

    private void style(){
        turnoP.setStyle(FONDOA);
        turnoS.setStyle(FONDOA);
        turnoU.setStyle(FONDOA);
        puestoP.setStyle(FONDOB);
        puestoS.setStyle(FONDOB);
        puestoU.setStyle(FONDOB);
        ico.setImage(new Image("/img/tcytc.png"));
    }
    
    
    private void videoRoulette(DoublyCircularLinkedList<String> pl, int i) throws OutOfRangeException{
        spinVideo(pl,i);
        md.setOnEndOfMedia(new Runnable() {
            int e = i;
            @Override
            public void run() {
                nextVideo(pl,e);
            }
        });
    }
    
    private void nextVideo(DoublyCircularLinkedList<String> pl, int e){
        try {
            if(!ac.isShowing()) System.exit(0);
            if(pl.get(e).equals(pl.getLast())){
                e = 0;
                videoRoulette(pl,e);
            }else videoRoulette(pl,++e);
        } catch (OutOfRangeException ex) {
            Logger logger = Logger.getLogger(AdminController.class);
            logger.error(ex.getMessage());
        }
    }
    
    private void spinVideo(DoublyCircularLinkedList<String> pl,int i) throws OutOfRangeException{
        File file = new File(pl.get(i));
        Media media = new Media(file.toURI().toString());
        md = new MediaPlayer(media);
        mediaV.setFitHeight(475);
        mediaV.setFitWidth(478);
        mediaV.setMediaPlayer(md);
        md.play();
    }

    public PriorityQueue<Paciente> getPq() {
        return pq;
    }

    public void setPq(PriorityQueue<Paciente> pq) {
        this.pq = pq;
    }

    public DoublyCircularLinkedList<String> getList() {
        return listVid;
    }

    public void pacienteAtendido(Turno tr){
        lturnos.remove(tr);
        lpuestos.offer(tr.getPuesto());
        actualizarSiguienteTurno();
    }
    
    public void setAtributtes(DoublyCircularLinkedList<String> list, Stage stage) {
        try {
            this.listVid = list;
            ac = stage;
            style();
            asignandoTiempo();
            int i =0;
            videoRoulette(list,i);
        } catch (OutOfRangeException ex) {
            Logger logger = Logger.getLogger(PantallaEsperaController.class);
            logger.error(ex.getMessage());
        }
    }
    
    private void actualizarSiguienteTurno(){
        while(!pq.isEmpty()&&!lpuestos.isEmpty()&&lturnos.size()<3){
            Paciente paciente = pq.poll();
            if(paciente.getEstado().equals(Estado.PENDIENTE)){
                Puesto puesto = lpuestos.poll();
                Turno turn = new Turno(paciente,puesto,crearTurno(paciente));
                lturnos.add(turn);
            }
        }
        for(int index = 0; index<=lturnos.size();index++)
            mostrandoTurnos(index);
    }
    
    public void actualizarTurnos(){
        while(!pq.isEmpty()&&!lpuestos.isEmpty()&&lturnos.size()<3){
            Paciente paciente = pq.poll();
            if(paciente.getEstado().equals(Estado.PENDIENTE)){
                Puesto puesto = lpuestos.poll();
                Turno turn = new Turno(paciente,puesto,crearTurno(paciente));
                lturnos.add(turn);
                mostrandoTurnos(lturnos.size());
                Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "SIGUIENTE", "Turnos    NOMBRE\n"+turn.getId()+"    "+paciente.getNombre());
            }
        }
    }
    
    private void mostrandoTurnos(int i){
        switch (i) {
            case 0:
                if(lturnos.isEmpty()) resetTurns(turnoP,puestoP);
                break;
            case 1:
                activandoTurnosPantalla(turnoP,puestoP,i-1);
                if(lturnos.size()<2) resetTurns(turnoS,puestoS);
                break;
            case 2:
                activandoTurnosPantalla(turnoS,puestoS,i-1);
                if(lturnos.size()<3) resetTurns(turnoU,puestoU);
                break;
            case 3:
                activandoTurnosPantalla(turnoU,puestoU,i-1);
                break;
            default:
                break;
        }
    }
    
    private void resetTurns(Label turnoP, Label puestoP){
        turnoP.setText("");
        turnoP.setVisible(false);
        puestoP.setText("");
        puestoP.setVisible(false);
    }
    
    private void activandoTurnosPantalla(Label turnoP, Label puestoP, int i){
        if(!turnoP.isVisible()) turnoP.setVisible(true);
        if(!puestoP.isVisible()) puestoP.setVisible(true);
        turnoP.setText(lturnos.get(i).getId());
        puestoP.setText(lturnos.get(i).getPuesto().getId()+"");
    }
    
    private String crearTurno(Paciente paciente){
        switch (paciente.getSintoma().getPrioridad()) {
            case 0:
                return "A"+(++cA);
            case 1:
                return "B"+(++cB);
            case 2:
                return "C"+(++cC);
            case 3:
                return "D"+(++cD);
            case 4:
                return "E"+(++cE);
            case 5:
                return "F"+(++cF);
            default:
                break;
        }
        return "";
    }
    
    private void asignandoTiempo(){
        Timer t = new Timer();
        TimerTask ts = new TimerTask(){
            @Override
            public void run() {
                LocalDateTime time = LocalDateTime.now();
                String hora = "0"+time.getHour();
                String minuto = "0"+time.getMinute();
                if(time.getHour()>=10) hora = time.getHour()+"";
                if(time.getMinute()>=10) minuto = time.getMinute()+"";
                tiempo.setText(hora+":"+minuto);
            }
        };
        t.schedule(ts, 0, 1000);
    }
}
