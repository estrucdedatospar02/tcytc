/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.*;
import ec.edu.espol.util.Lectura;
import ec.edu.espol.util.Proteccion;
import ec.edu.espol.util.Sistema;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

/**
 * FXML Controller class
 *
 * @author Andrés Medina Jácome
 */
public class AsignarPuestoController implements Initializable {

    @FXML
    private TextArea infoD;
    @FXML
    private ComboBox<Medico> cbDoctor;
    @FXML
    private VBox vPuestos;
    @FXML
    private Button btnAgregar;
    @FXML
    private Button btnQuitar;

    private PantallaEsperaController pec;
    private LinkedList<Puesto> lPuesto;
    private static final String AVISO = "Asignar Médico";
    
    @FXML
    private ComboBox<Integer> numPuesto;
    
    public PantallaEsperaController getPec() {
        return pec;
    }    

    public void setPec(PantallaEsperaController pec) {
        this.pec = pec;
    }
    
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        LinkedList<Integer> puestos= new LinkedList<>();
        int i = 1;
        agregarPuestos(puestos, i);
        numPuesto.setItems(FXCollections.observableList(puestos));
        agregarDoctores();
        mostrarPuestos();
    }

    private void agregarDoctores(){
        try {
            LinkedList<Medico> lDoctor = (LinkedList<Medico>) Lectura.leerMedicos();
            if(!lDoctor.isEmpty()) cbDoctor.setItems(FXCollections.observableList(lDoctor));
            else{
                Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Mostrar Medicos", "No se han registrado médicos en esta clínica", null);
                cbDoctor.setDisable(true);
            }
        } catch (IOException | ClassNotFoundException ex) {
            Logger logger = Logger.getLogger(AsignarPuestoController.class);
            logger.error(ex.getMessage());
        }
    }
    
    private void agregarPuestos(LinkedList<Integer> puestos,int i){
        puestos.add(i);
        if(puestos.size()<10)   agregarPuestos(puestos, ++i);
    }
    
    private void mostrarPuestos(){
        try {
            lPuesto = (LinkedList<Puesto>)Lectura.leerPuestos();
            if(!lPuesto.isEmpty()){
                for(Puesto puesto: lPuesto)
                    vPuestos.getChildren().add(new Text(puesto.toString()));
            }
            else{
                TextArea tx = new TextArea("No se ha encontrado ni un puesto creado.\nSeleccion un medico en la seccion derecha (->) y de click en agregar para crear un puesto.\nMuchas gracias.");
                tx.setWrapText(true);
                tx.setEditable(false);
                vPuestos.getChildren().add(tx);
            }
        } catch (IOException | ClassNotFoundException ex) {
            Logger logger = Logger.getLogger(AsignarPuestoController.class);
            logger.error(ex.getMessage());
        }
    }
    
    @FXML
    private void agregar(MouseEvent event) {
        if(cbDoctor.getValue()==null) Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), AVISO, "No se ha escogido un médico", "Asegúrese de escoger un médico\nantes de agregar un puesto");
        else if(numPuesto.getValue()==null) Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), AVISO, "No se ha escogido un puesto", "Asegúrese de escoger un puesto \nantes de dar click en agregar");
        else if(Proteccion.guardarPuesto(lPuesto, new Puesto(numPuesto.getValue(),cbDoctor.getValue()))){
            pec.getLpuestos().offer(new Puesto(numPuesto.getValue(),cbDoctor.getValue()));
            pec.actualizarTurnos();
            Stage st = (Stage) btnAgregar.getScene().getWindow();
            Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), AVISO, "Éxito!", "Se ha asignado correctamente al Dr(a). " + cbDoctor.getValue().getApellido()+"\nal puesto "+numPuesto.getValue());
            st.close();
        }else Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), AVISO, "Puesto ocupado");
    }

    @FXML
    private void quitar(MouseEvent event) {
        Puesto p = new Puesto(numPuesto.getValue());
        if(lPuesto.contains(p)){
            p = lPuesto.get(lPuesto.indexOf(p));
            p.asignarMedico(null);
            lPuesto.remove(p);
            Proteccion.guardarPuesto(lPuesto);
            Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), AVISO, "Puesto eliminado con éxito");
            vPuestos.getChildren().clear();
            infoD.setText("");
            mostrarPuestos();
        }else Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), AVISO, "Puesto vacío");
    }

    @FXML
    private void seleccion(ActionEvent event) {
        if(!infoD.getText().equals("")) infoD.setText("");
        if(numPuesto.getValue()==null) Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), AVISO, "Asegúrese de seleccionar un puesto");
        else{
            Puesto puesto = seleccionarPuesto(numPuesto.getValue());
            if(puesto!=null) infoD.setText(puesto.getMedico().toString());
            btnQuitar.setDisable(false);
        }
        
    }
    
    private Puesto seleccionarPuesto(int valor){
        for(Puesto puesto: lPuesto)
            if(puesto.getId()==valor) return puesto;
        return null;
    }
}
