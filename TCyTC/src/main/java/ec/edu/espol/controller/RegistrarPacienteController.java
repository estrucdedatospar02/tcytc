/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.util.ExistenceException;
import ec.edu.espol.model.*;
import ec.edu.espol.util.*;
import java.io.IOException;
import java.net.URL;
import java.util.*;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

/**
 * FXML Controller class
 *
 * @author Andrés Medina Jácome
 */
public class RegistrarPacienteController implements Initializable {

    @FXML
    private TextField tNombre;
    @FXML
    private TextField tApellido;
    @FXML
    private TextField tEdad;
    @FXML
    private TextField sintom;
    @FXML
    private ComboBox<Genero> cGenero;
    @FXML
    private ComboBox priority;
    @FXML
    private ComboBox<Sintoma> cbSintomas;
    @FXML
    private RadioButton checkSin;
    
    private PantallaEsperaController pec;
    private LinkedList<Sintoma> lSintoma;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            lSintoma = (LinkedList<Sintoma>) Indicio.cargarSintomas();
            if(lSintoma.isEmpty()) cbSintomas.setDisable(true);
            else    cbSintomas.setItems(FXCollections.observableList(lSintoma));
        } catch (IOException ex) {
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR),"Cargando Sintomas","No hay información en el archivo");
            lSintoma = new LinkedList<>();
        }
        agregandoComboPrioridad();
        Sistema.agregandoComboGenero(cGenero);
        Sistema.limitandoTexto(tNombre,tApellido,tEdad,sintom);
    }
    
    private void agregandoComboPrioridad(){
        ArrayList<Integer> listaPrio = new ArrayList<>();
        listaPrio.add(0);
        listaPrio.add(1);
        listaPrio.add(2);
        listaPrio.add(3);
        listaPrio.add(4);
        listaPrio.add(5);
        priority.setItems(FXCollections.observableList(listaPrio));
    }

    @FXML
    private void registrar(MouseEvent event) {
        if(tNombre.getText().equals("")||tApellido.getText().equals("")||tEdad.getText().equals("")||((cbSintomas.getValue()==null&&!cbSintomas.isDisable())&&(sintom.getText().equals("")&&!sintom.isDisable()))||cGenero.getValue()==null||(priority.getValue()==null&&!priority.isDisable())) Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Datos ingresados", "No ha ingresado correctamente los datos", "Verifique que llene todos los espacios");
        else{
            try {
                Genero sexo = cGenero.getValue();
                LinkedList<Paciente> pacientes = (LinkedList<Paciente>) Lectura.leerPacientes();
                registrandoPaciente(pacientes, sexo);
            } catch (ExistenceException | IOException | ClassNotFoundException ex) {
                Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Registrar Paciente", ex.getMessage());
                Logger logger = Logger.getLogger(AsignarPuestoController.class);
                logger.error(ex.getMessage());
            }
        }
    }
    
    private void registrandoPaciente(LinkedList<Paciente> pacientes, Genero sexo) throws ExistenceException, IOException{
        Sintoma sintoma;
        if(cbSintomas.getValue()!=null&&!cbSintomas.isDisable()) sintoma = cbSintomas.getValue();
        else{ 
            sintoma = new Sintoma(sintom.getText(),Integer.parseInt(priority.getValue().toString()));
            if(lSintoma.contains(sintoma)) throw new ExistenceException("El sintoma ingresado\nya se encuentra en el sistema.\nDar click en la selección");
        }
        Paciente paciente = new Paciente(tNombre.getText(),tApellido.getText(),Integer.parseInt(tEdad.getText()),sexo,sintoma);
        if(!Proteccion.guardarPaciente(pacientes, paciente))   throw new ExistenceException("El paciente ingresado\nya se encuentra en el sistema");
        if(!lSintoma.contains(sintoma)) Indicio.guardarSintomas(sintoma);
        pec.getPq().offer(paciente);
        pec.actualizarTurnos();
        Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Registro Paciente", "Éxito!","El paciente acaba de ser registrado en el sistema\nY se encuentra en espera de atención");
        Stage st = (Stage) this.cGenero.getScene().getWindow();
        st.close();
    }

    public PantallaEsperaController getPec() {
        return pec;
    }

    public void setPec(PantallaEsperaController pec) {
        this.pec = pec;
    }

    @FXML
    private void activarTexto(ActionEvent event) {
        if(checkSin.isSelected()){
            sintom.setDisable(false);
            priority.setDisable(false);
            if(!lSintoma.isEmpty()) cbSintomas.setDisable(true);
        }
        else{
            sintom.setDisable(true);
            priority.setDisable(true);
            if(cbSintomas.isDisable()&&!lSintoma.isEmpty()) cbSintomas.setDisable(false);
        }
    }
    
    
}
