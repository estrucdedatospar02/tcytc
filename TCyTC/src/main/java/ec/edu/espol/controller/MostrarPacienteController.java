/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.*;
import ec.edu.espol.util.Lectura;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.ResourceBundle;
import javafx.collections.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import org.apache.log4j.Logger;

/**
 * FXML Controller class
 *
 * @author Andrés Medina Jácome
 */
public class MostrarPacienteController implements Initializable {

    @FXML
    private TableColumn cNombre;
    @FXML
    private TableColumn cApellido;
    @FXML
    private TableColumn cEdad;
    @FXML
    private TableColumn cGenero;
    @FXML
    private TableColumn cSintoma;
    @FXML
    private TableView<Paciente> tbPaciente;
    @FXML
    private TableColumn cEstado;
    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            ObservableList<Paciente> pacientes = FXCollections.observableArrayList();
            LinkedList<Paciente> lpacientes = (LinkedList<Paciente>)Lectura.leerPacientes();
            pacientes.addAll(lpacientes);
            this.cNombre.setCellValueFactory(new PropertyValueFactory("nombre"));
            this.cApellido.setCellValueFactory(new PropertyValueFactory("apellido"));
            this.cEdad.setCellValueFactory(new PropertyValueFactory("edad"));
            this.cGenero.setCellValueFactory(new PropertyValueFactory("genero"));
            this.cSintoma.setCellValueFactory(new PropertyValueFactory("sintoma"));
            this.cEstado.setCellValueFactory(new PropertyValueFactory("estado"));
            this.tbPaciente.setItems(pacientes);
        } catch (IOException | ClassNotFoundException ex) {
            Logger logger = Logger.getLogger(MostrarPacienteController.class);
            logger.error(ex.getMessage());
        }
    }    
    
}

