/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.controller;

import ec.edu.espol.model.*;
import ec.edu.espol.util.*;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.*;
import javafx.fxml.*;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import org.apache.log4j.Logger;

/**
 * FXML Controller class
 *
 * @author Andrés Medina Jácome
 */
public class RegistrarMedicoController implements Initializable {

    @FXML
    private TextField tNombre;
    @FXML
    private TextField tApellido;
    @FXML
    private TextField tEdad;
    @FXML
    private ComboBox<Genero> cGenero;
    @FXML
    private TextField tEspecialidad;

    /**
     * Initializes the controller class.
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Sistema.agregandoComboGenero(cGenero);
        Sistema.limitandoTexto(tNombre,tApellido,tEdad,tEspecialidad);
    }
    
    @FXML
    private void registrar(MouseEvent event) {
        if(tNombre.getText().equals("")||tApellido.getText().equals("")||tEdad.getText().equals("")||cGenero.getValue()==null||tEspecialidad.getText().equals("")) Sistema.showMessage(new Alert(Alert.AlertType.ERROR), "Datos ingresados", "No ha ingresado correctamente los datos", "Verifique que llene todos los espacios");
        else{
            try {
                Genero sexo = cGenero.getValue();
                LinkedList<Medico> medicos = (LinkedList<Medico>) Lectura.leerMedicos();
                LocalDate date = LocalDate.now();
                Medico medico = new Medico(tEspecialidad.getText(),tNombre.getText(),tApellido.getText(),Integer.parseInt(tEdad.getText()),sexo,Integer.parseInt(date.getYear()+""+date.getMonthValue()+date.getDayOfMonth()+0+(medicos.size()+1)));
                if(Proteccion.guardarMedico(medicos, medico)){
                    Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Registro Medico", "Éxito!","El medico acaba de ser registrado en el sistema");
                    Stage st = (Stage) this.cGenero.getScene().getWindow();
                    st.close();
                }else Sistema.showMessage(new Alert(Alert.AlertType.INFORMATION), "Registrar Medico", "El medico ingresado ya se encuentra en el sistema");
            } catch (IOException | ClassNotFoundException ex) {
                Logger logger = Logger.getLogger(RegistrarMedicoController.class);
                logger.error(ex.getMessage());
            }
        }
    }
    
}
