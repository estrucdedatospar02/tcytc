/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.Serializable;

/**
 *
 * @author Sebastián Elías Benalcázar García
 */
public class Persona implements Serializable{
    
    protected String nombre;
    protected String apellido;
    protected int edad;
    protected Genero genero;

    public Persona(String nombre, String apellido, int edad, Genero genero) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
        this.genero = genero;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public int getEdad() {
        return edad;
    }

    public Genero getGenero() {
        return genero;
    }
    
}
