/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.Serializable;

/**
 *
 * @author Sebastián Elías Benalcázar García
 */
public class Consulta implements Serializable{
    
    private int id;
    private final String diagnostico;
    private final String receta;

    public Consulta(String diagnostico, String receta, int id) {
        this.diagnostico = diagnostico;
        this.receta = receta;
        this.id = id;
    }

    public String getDiagnostico() {
        return diagnostico;
    }

    public String getReceta() {
        return receta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    @Override
    public String toString() {
        return diagnostico + "\n\nReceta: " + receta;
    }
    
}
