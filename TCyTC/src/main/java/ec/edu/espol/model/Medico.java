/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import ec.edu.espol.util.ExistenceException;
import java.io.*;
import java.util.*;

/**
 *
 * @author Sebastián Elías Benalcázar García
 */
public class Medico extends Persona implements Serializable{
    
    private String especialidad;
    private int id;

    public Medico(String especialidad, String nombre, String apellido, int edad, Genero genero, int id) {
        super(nombre, apellido, edad, genero);
        this.especialidad = especialidad;
        this.id = id;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String atenderPaciente(Paciente paciente, LinkedList<String> recetas, String diagnos) throws ExistenceException{
        String receta = recetar(recetas);
        String diagnostico = "Sintoma: "+paciente.getSintoma().getDescripcion()+"\nDiagnóstico: "+diagnos;
        return registrarConsulta(paciente,diagnostico,receta);
    }
    
    
    private String registrarConsulta(Paciente paciente, String diagnostico, String receta) throws ExistenceException{
        if (receta==null||diagnostico==null||paciente==null) throw new ExistenceException("Asegúrese de completar la información necesaria");
        Consulta consul = new Consulta(diagnostico,receta,paciente.getHistorial().size()+1);
        paciente.getHistorial().addLast(consul);
        paciente.setEstado(Estado.ATENDIDO);
        return consul.toString();
    }
    
    private static String recetar(LinkedList<String> recetas){
        StringBuilder str = new StringBuilder();
        for(String receta : recetas){
            str.append("\n");
            str.append(receta);
        }
        return str.toString();
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.especialidad,this.getNombre(),this.getApellido(),this.getEdad(),this.getGenero());
    }
    
    @Override
    public String toString() {
        return "Nombre: " + nombre + "\nApellido: " + apellido + "\nEspecialidad: " + especialidad + "\nNúmero de registro en clínica: " + id;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Medico other = (Medico)obj;
        return Objects.equals(this.getNombre(), other.getNombre())&&Objects.equals(this.getApellido(), other.getApellido())&&Objects.equals(this.especialidad, other.especialidad);
    }
    
}