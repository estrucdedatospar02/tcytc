/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Sebastián Elías Benalcázar García
 */
public class Sintoma implements Serializable{
    
    private String descripcion;
    private int prioridad;

    public Sintoma(){}
    
    public Sintoma(String descripcion, int prioridad) {
        this.descripcion = descripcion;
        this.prioridad = prioridad;
    }
    
    public Sintoma(int prioridad) {
        this.prioridad = prioridad;
    }
    
    public String getDescripcion() {
        return descripcion;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }
    
    @Override
    public String toString() {
        return descripcion;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 23 * hash + Objects.hashCode(this.descripcion);
        hash = 23 * hash + this.prioridad;
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Sintoma other = (Sintoma)obj;
        return Objects.equals(this.descripcion, other.descripcion)&&this.prioridad==other.prioridad;
    }
}
