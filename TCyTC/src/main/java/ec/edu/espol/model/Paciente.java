/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.*;
import java.util.*;

/**
 *
 * @author Sebastián Elías Benalcázar García
 */
public class Paciente extends Persona implements Serializable,Comparable<Paciente>{
    
    private Sintoma sintoma;
    private LinkedList<Consulta> historial;
    private Estado estado;
    
    public Paciente(String nombre, String apellido, int edad, Genero genero, Sintoma sintoma) {
        super(nombre, apellido, edad, genero);
        this.sintoma = sintoma;
        this.historial = new LinkedList<>();
        this.estado = Estado.PENDIENTE;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    
    public Sintoma getSintoma() {
        return sintoma;
    }

    public LinkedList<Consulta> getHistorial() {
        return historial;
    }

    public boolean separarCita(PriorityQueue<Paciente> pacientes){
        return pacientes.contains(this);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.nombre,this.apellido,this.edad,this.historial,this.sintoma,this.genero,this.estado);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj.getClass()!=this.getClass())
            return false;
        Paciente other = (Paciente)obj;
        return Objects.equals(this.getNombre(), other.getNombre())&&Objects.equals(this.getApellido(), other.getApellido())&&this.edad==other.edad;
    }

    @Override
    public String toString() {
        return "nombre: " + nombre + ", apellido: " + apellido + ", edad: "+ edad + ", genero: "+ genero + ", sintoma: " + sintoma;
    }

    @Override
    public int compareTo(Paciente o) {
        if(sintoma.getPrioridad()>o.getSintoma().getPrioridad()) return 1;
        if(sintoma.getPrioridad()<o.getSintoma().getPrioridad()) return -1;
        return 0;
    }
    
}
