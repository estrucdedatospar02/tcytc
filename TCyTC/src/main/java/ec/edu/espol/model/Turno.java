/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.util.Objects;

/**
 *
 * @author Sebastián Elías Benalcázar García
 */
public class Turno {
    
    private String id;
    private Paciente paciente;
    private Puesto puesto;

    public Turno(String id) {
        this.id = id;
    }

    public Turno(Puesto puesto, String id) {
        this.id = id;
        this.puesto = puesto;
    }

    public Turno(Paciente paciente, Puesto puesto) {
        this.paciente = paciente;
        this.puesto = puesto;
    }
    
    public Turno(Paciente paciente, Puesto puesto, String id) {
        this.id = id;
        this.paciente = paciente;
        this.puesto = puesto;
    }

    public String getId() {
        return id;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public Puesto getPuesto() {
        return puesto;
    }

    public void asignarPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public void asignarPuesto(Puesto puesto) {
        this.puesto = puesto;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + Objects.hashCode(this.id);
        hash = 97 * hash + Objects.hashCode(this.paciente);
        hash = 97 * hash + Objects.hashCode(this.puesto);
        return hash;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Turno other = (Turno) obj;
        return Objects.equals(this.paciente, other.paciente)&&Objects.equals(this.puesto, other.puesto);
    }

    @Override
    public String toString() {
        return "" + puesto.getId();
    }
    
}