/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.model;

import java.io.Serializable;
import java.util.Objects;

/**
 *
 * @author Sebastián Elías Benalcázar García
 */
public class Puesto implements Serializable{
    
    private int id;
    private Medico medico;

    public Puesto(int id) {
        this.id = id;
    }
    
    public Puesto(int id, Medico medico) {
        this.id = id;
        this.medico = medico;
    }

    public int getId() {
        return id;
    }

    public Medico getMedico() {
        return medico;
    }

    public void asignarMedico(Medico medico) {
        this.medico = medico;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + this.id;
        hash = 79 * hash + Objects.hashCode(this.medico);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Puesto other = (Puesto) obj;
        return this.id==other.id||Objects.equals(this.medico, other.medico);
    }

    @Override
    public String toString() {
        if(medico.genero.equals(Genero.MASCULINO)) return "El Dr. " + medico.getNombre() + " está encargado del puesto " + id ;
        else if(medico.genero.equals(Genero.FEMENINO)) return "La Dra. " + medico.getNombre() + " está encargada del puesto " + id ;
        return "El Dr(a). " + medico.getNombre() + " está encargado del puesto " + id ;
    }
    
    
}
