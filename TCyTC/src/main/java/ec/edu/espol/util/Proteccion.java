/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import ec.edu.espol.model.*;
import java.io.IOException;
import java.util.List;
import javafx.scene.control.Alert;

/**
 *
 * @author Andrés Medina Jácome
 */
public class Proteccion {
    
    private Proteccion(){}
    
    private static final String ANUNCIOG = "ERROR AL GUARDAR";
    private static final String ANUNCIOA = "ERROR AL ACTUALIZAR";
    private static final String TITULO = "ARCHIVO";
    
    
    public static boolean guardarPaciente(List<Paciente> pacientes, Paciente paciente){
        try {
            Registro registro = new Registro("pacientes.ser");
            return registro.registrar(pacientes, paciente);
        } catch (IOException ex) {
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR), TITULO, ANUNCIOG, "El archivo de los pacientes no se pudo guardar");
            return false;
        }
    }
    
    public static boolean guardarPaciente(List<Paciente> pacientes){
        try {
            Registro registro = new Registro("pacientes.ser");
            return registro.registrar(pacientes);
        } catch (IOException ex) {
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR), TITULO, ANUNCIOA, "El archivo de los pacientes no se pudo actualizar");
            return false;
        }
    }
    
    public static boolean guardarMedico(List<Medico> medicos, Medico medico){
        try {
            Registro registro = new Registro("medicos.ser");
            return registro.registrar(medicos, medico);
        } catch (IOException ex) {
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR), TITULO, ANUNCIOG, "El archivo de los medicos no se pudo guardar");
            return false;
        }
    }
    
    public static boolean guardarMedico(List<Medico> medicos){
        try {
            Registro registro = new Registro("medicos.ser");
            return registro.registrar(medicos);
        } catch (IOException ex) {
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR), TITULO, ANUNCIOA, "El archivo de los pacientes no se actualizar");
            return false;
        }
    }
    
    public static boolean guardarPuesto(List<Puesto> puestos, Puesto puesto){
        try {
            Registro registro = new Registro("puestos.ser");
            return registro.registrar(puestos, puesto);
        } catch (IOException ex) {
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR), TITULO, ANUNCIOG, "El archivo de los puestos no se pudo guardar");
            return false;
        }
    }
    
    public static boolean guardarPuesto(List<Puesto> puestos){
        try {
            Registro registro = new Registro("puestos.ser");
            return registro.registrar(puestos);
        } catch (IOException ex) {
            Sistema.showMessage(new Alert(Alert.AlertType.ERROR), TITULO, ANUNCIOA, "El archivo de los puestos no se pudo actualizar");
            return false;
        }
    }
}
