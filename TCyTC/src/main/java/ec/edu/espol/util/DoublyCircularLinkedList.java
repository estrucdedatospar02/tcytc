package ec.edu.espol.util;


import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Andrés Medina Jácome
 */
public class DoublyCircularLinkedList<E> implements List<E>,Iterable<E>{
    private Node<E> last;
    private int current;
    private class Node<E>{
        private E data;
        private Node<E> next;
        private Node<E> previous;    
        
        public Node(E data){
            this.data = data;
            this.next=this;
            this.previous=this;
        }
    }

    @Override
    public boolean addFirst(E element) {
        if(element == null) return false;
        Node<E> n = new Node<>(element);
        if(isEmpty()) last=n;
        else{
            n.next = last.next;
            last.next.previous=n;
            n.previous=last;
            last.next = n;
        }
        current++;
        return true;
    }

    @Override
    public boolean addLast(E element) {
        if(element == null) return false;
        Node<E> n = new Node<>(element);
        if(isEmpty()) last=n;
        else{
            n.next=last.next;
            n.previous = last;
            last.next.previous=n;
            last.next = n;
            last = n;
        }
        current++;
        return true;
    }

    @Override
    public boolean add(E element, int index) {
        if(index == 0||isEmpty()) return addFirst(element);
        if(element == null||index>=current) return false;
        
        Node<E> n = new Node<>(element);
        if(isEmpty()) last=n;
        else addNode(n,index);
        current++;
        return true;
    }
    
    private void addNode(Node<E> n, int index){
        Node<E> e = last.next;
        for(int i =0; i<index;i++) e=e.next;
        n.next = e;
        n.previous = e.previous;
        e.previous.next=n;
        e.previous=n;
    }

    @Override
    public E get(int index) throws OutOfRangeException{
        if(isEmpty()) throw new OutOfRangeException("No hay elementos en la lista");
        if(index<0||index>=current) throw new OutOfRangeException("Indice ingresa está fuera del rango de la lista");
        Node<E> n = getNode(index);
        return n.data;
    }

    @Override
    public int size() {
        return current;
    }

    @Override
    public boolean isEmpty() {
        return last==null;
    }

    @Override
    public boolean remove(int index) {
        if(index==0) return removeFirst();
        else if(index==current-1) return removeLast();
        else if(isEmpty()||index>=current) return false;
        if(last.next==last){
            last.data = null;
            last = null;
        }else removeNode(index);
        current--;
        return true;
    }

    private void removeNode(int index){
        Node<E> nod = getNode(index);
        nod.previous.next = nod.next;
        nod.next.previous=nod.previous;
        nod.data = null;
        nod.next = null;
        nod.previous = null;
    }
    
    @Override
    public boolean contains(E element) {
        if(element==null) return false;
        Node<E> i = last.next;
        do{
            if(i.data.equals(element)) return true;
            i=i.next;
        }while(i!=last.next);
        return false;
    }

    @Override
    public boolean removeFirst() {
        if(isEmpty()) return false;
        if(last.next==last){
            last.data = null;
            last = null;
        }else{
            Node<E> tmp = last.next;
            last.next.next.previous=last;
            last.next = last.next.next;
            tmp.data = null;
            tmp.next = null;
            tmp.previous = null;
        }current--;
        return true;
    }

    @Override
    public boolean removeLast() {
        if(isEmpty()) return false;
        if(last.next==last){
            last.data = null;
            last = null;
        }else{
            last.next.previous=last.previous;
            last.previous.next=last.next;
            last.next= null;
            last.data = null;
            last = last.previous;
            last.previous = null;
        }current--;
        return true;
    }

    @Override
    public E getFirst() throws OutOfRangeException{
        if(isEmpty()) throw new OutOfRangeException("Lista totalmente vacía");
        return last.next.data;
    }

    @Override
    public E getLast() throws OutOfRangeException{
        if(isEmpty()) throw new OutOfRangeException("No existe ni un elemento en la lista");
        return last.data;
    }
    
    private Node<E> getNode(int index){
        Node<E> n = last.next;
        for(int i = 0; i<index;i++){
            n=n.next;
        }
        return n;
    }
    @Override
    public String toString(){
        if(isEmpty()) return "[]";
        StringBuilder str = new StringBuilder();
        str.append("[");
        Node<E> i = last.next;
        for(int e =0; e<current-1;e++){
            str.append(i.data);
            str.append(",");
            i=i.next;
        }
        str.append(last.data);
        str.append("]");
        return str.toString();
    }
    
    @Override
    public Iterator<E> iterator(){
        return new Iterator<E>(){
            private Node<E> i = last.next;
            @Override
            public boolean hasNext() {
                return i!=null;
            }
            @Override
            public E next() {
                E tmp = i.data;
                if(i.next==null) throw new NoSuchElementException();
                i = i.next;
                return tmp;
            }
        };
    }
    
    public ListIterator<E> listIterator(int index){
        return new ListIterator<E>(){
            private Node<E> i = getNode(index);
            private int crrnt = index;
            @Override
            public boolean hasNext() {
                return i!=null;
            }

            @Override
            public E next() {
                E tmp = i.data;
                if(i.next==null) throw new NoSuchElementException();
                i = i.next;
                crrnt++;
                return tmp;
            }

            @Override
            public boolean hasPrevious() {
                return i!=null;
            }

            @Override
            public E previous() {
                E tmp = i.data;
                i = i.previous;
                crrnt--;
                return tmp;
            }

            @Override
            public int nextIndex() {
                return crrnt+1;
            }

            @Override
            public int previousIndex() {
                return crrnt-1;
            }

            @Override
            public void remove() {
                i.data = null;
                Node<E> prv = i.previous;
                i.next.previous=prv;
                prv.next = i.next;
                i.next = i.previous = null;
            }

            @Override
            public void set(E e) {
                i.data = e;
            }

            @Override
            public void add(E e) {
                Node<E> n = new Node<>(e);
                n.previous = i.previous;
                i.previous.next=n;
                n.next = i;
                i.previous=n;
            }
        };
    }
}
