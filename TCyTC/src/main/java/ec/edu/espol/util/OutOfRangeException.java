package ec.edu.espol.util;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Andrés Medina Jácome
 */
public class OutOfRangeException extends Exception {

    public OutOfRangeException(String s) {
        super(s);
    }
    
}
