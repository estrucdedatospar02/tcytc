/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Andrés Medina Jácome
 */
public class Registro<E> {
    
    private final File file;
    
    protected Registro(String name){
        file = new File(name);
    }
    
    protected boolean registrar(List<E> personas, E persona) throws IOException {
        if(!file.exists()){ 
            FileWriter fw = new FileWriter(file);
            fw.close();
        }
        if(personas.contains(persona)) return false;
        personas.add(persona);
        try(ObjectOutputStream a = new ObjectOutputStream(new FileOutputStream(file.getPath()))){
            a.writeObject((LinkedList) personas);
            return true;
        }
        
    }
    
    protected boolean registrar(List<E> personas) throws IOException {
        if(!file.exists()){
            FileWriter fw = new FileWriter(file);
            fw.close();
        }
        try(ObjectOutputStream a = new ObjectOutputStream(new FileOutputStream(file))){
            a.writeObject((LinkedList) personas);
            return true;
        }
    }
    
}
