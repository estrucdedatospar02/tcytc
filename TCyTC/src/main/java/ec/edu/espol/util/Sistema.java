/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import ec.edu.espol.gui.App;
import ec.edu.espol.model.Genero;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Alert;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;

/**
 *
 * @author Andrés Medina Jácome
 */
public class Sistema{

    private Sistema() {}
    
    private static final String MATCHSTR = "[a-zA-Z\\s]{0,25}";
    
    public static void showMessage(Alert show,String titulo, String encabezado, String mensaje){
        show.setHeaderText(encabezado);
        show.setTitle(titulo);
        show.setContentText(mensaje);
        show.showAndWait();
    }
    
    public static void showMessage(Alert show,String titulo, String encabezado){
        show.setHeaderText(encabezado);
        show.setTitle(titulo);
        show.showAndWait();
    }
    
    public static FXMLLoader loadFXML(String fxml){
        return new FXMLLoader(App.class.getResource("/ec/edu/espol/gui/"+fxml + ".fxml"));
    }
    
    public static void agregandoComboGenero(ComboBox<Genero> cGenero){
        ArrayList<Genero> genero = new ArrayList<>();
        genero.add(Genero.MASCULINO);
        genero.add(Genero.FEMENINO);
        genero.add(Genero.OTRO);
        cGenero.setItems(FXCollections.observableArrayList(genero));
    }
    public static void limitandoTexto(TextField tNombre,TextField tApellido,TextField tEdad,TextField tEspecialidad){
        tNombre.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches(MATCHSTR))? condicion: null ));
        tApellido.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches(MATCHSTR))? condicion: null ));
        tEdad.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches("[1]{0,1}[0-9]{0,1}[0-9]{0,1}"))? condicion: null ));
        tEspecialidad.setTextFormatter(new TextFormatter<>(condicion -> (condicion.getControlNewText().matches(MATCHSTR))? condicion: null ));
    }
    
}
