/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.*;
import ec.edu.espol.model.Sintoma;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Andrés Medina Jácome
 */
public class Indicio {
    
    private Indicio(){}
    
    public static List<Sintoma> cargarSintomas() throws IOException{
        File file = new File("sintomas.txt");
        if(!file.exists()){
            FileWriter fw = new FileWriter(file);
            fw.close();
        }
        try(BufferedReader wr = new BufferedReader(new FileReader(file))){
            List<Sintoma> listaS = new LinkedList<>();
            String linea;
            while((linea = wr.readLine())!=null){
                String[] lin = linea.split("\\|");
                listaS.add(new Sintoma(lin[0],Integer.parseInt(lin[1])));
            }
            return listaS;
        }
    }
    
    public static boolean guardarSintomas(Sintoma sintoma) throws IOException{
        File file = new File("sintomas.txt");
        if(!file.exists()){
            FileWriter fw = new FileWriter(file);
            fw.close();
        }
        try(BufferedWriter wr= new BufferedWriter(new FileWriter(file,true))){
            wr.write(sintoma.getDescripcion()+"|"+sintoma.getPrioridad()+"\n");
            return true;
        }
    }
    
}
