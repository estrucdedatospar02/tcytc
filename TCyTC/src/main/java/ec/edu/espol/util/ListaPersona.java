/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.*;
import java.util.LinkedList;

/**
 *
 * @author Andrés Medina Jácome
 * @param <E>
 */
public class ListaPersona<E>{
    
    private File file;
    
    protected ListaPersona(String nombre){
        file = new File(nombre);
    }
    
    protected LinkedList<E> desserializar() throws IOException, ClassNotFoundException {
        if(file.exists()){
            try(ObjectInputStream ob = new ObjectInputStream(new FileInputStream(file))){
                return (LinkedList<E>) ob.readObject();
            }
        }return new LinkedList<>();
    }
}


