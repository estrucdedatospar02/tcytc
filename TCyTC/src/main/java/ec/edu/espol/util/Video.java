/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import java.io.*;

/**
 *
 * @author Andrés Medina Jácome
 */
public class Video {
    
    private Video(){}
    
    public static DoublyCircularLinkedList<String> cargarVideos() throws IOException{
        File file = new File("videos.txt");
        if(file.exists()){
            try(BufferedReader wr = new BufferedReader(new FileReader("videos.txt"))){
                DoublyCircularLinkedList<String> listaS = new DoublyCircularLinkedList<>();
                String linea;
                while((linea = wr.readLine())!=null){
                    listaS.addLast(linea);
                }
                return listaS;
            }
        } return new DoublyCircularLinkedList<>();
    }
    
}
