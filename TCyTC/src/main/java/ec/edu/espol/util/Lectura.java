/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.espol.util;

import ec.edu.espol.model.*;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author Andrés Medina Jácome
 */
public class Lectura {
    
    private Lectura(){}
    
    public static List<Paciente> leerPacientes() throws IOException, ClassNotFoundException{
        ListaPersona<Paciente> p = new ListaPersona<>("pacientes.ser");
        return p.desserializar();
    }
    
    public static List<Medico> leerMedicos() throws IOException, ClassNotFoundException{
        ListaPersona<Medico> m = new ListaPersona<>("medicos.ser");
        return m.desserializar();
    }
    
    public static List<Puesto> leerPuestos() throws IOException, ClassNotFoundException{
        ListaPersona<Puesto> p = new ListaPersona<>("puestos.ser");
        return p.desserializar();
    }
}
