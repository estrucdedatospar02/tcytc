package ec.edu.espol.gui;

import ec.edu.espol.controller.AdminController;
import ec.edu.espol.util.Sistema;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.*;
import javafx.stage.Stage;

import java.io.IOException;
import javafx.scene.image.Image;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Scene scene;
    
    @Override
    public void start(Stage stage) throws IOException {
        FXMLLoader fmxl = Sistema.loadFXML("admin");
        Parent form = fmxl.load();
        AdminController ec = fmxl.getController();
        App.setScene(new Scene(form));
        stage.setScene(scene);
        stage.setTitle("TCyTC v1.1.2020");
        stage.getIcons().add(new Image("/img/tcytc.png"));
        stage.resizableProperty().setValue(Boolean.FALSE);
        stage.show();
        ec.setAtributtes(stage);
    }

    public static void setRoot(String fxml) throws IOException {
        scene.setRoot(loadFXML(fxml));
    }

    public static void setRoot(Parent fxml) throws IOException {
        scene.setRoot(fxml);
    }
    
    private static Parent loadFXML(String fxml) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/ec/edu/espol/gui/"+fxml + ".fxml"));
        return fxmlLoader.load();
    }

    public static void main(String[] args) {
        launch();
    }
    
    public static void setScene(Scene scene) {
        App.scene = scene;
    }
    
}