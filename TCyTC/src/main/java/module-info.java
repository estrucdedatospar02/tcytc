module ec.edu.espol.gui {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.media;
    requires java.base;
    requires log4j;

    opens ec.edu.espol.gui to javafx.fxml;
    exports ec.edu.espol.gui;
}